package com.example.exam11.util


class Constants {

    companion object {
        const val BASE_URL = "https://run.mocky.io/"
        const val ENDPOINT = "v3/80d25aee-d9a6-4e9c-b1d1-80d2a7c979bf"
    }
}