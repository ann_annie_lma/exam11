package com.example.exam11.reprository

import com.example.exam11.api.RemoteDataSource
import com.example.exam11.model.Message
import com.example.exam11.util.BaseApiResponse
import com.example.exam11.util.NetworkResult
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.Dispatcher
import javax.inject.Inject

@ActivityRetainedScoped
class MessengerRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : BaseApiResponse() {

    suspend fun getMessages(): Flow<NetworkResult<List<Message>>> {

        return flow<NetworkResult<List<Message>>> {
            emit(safeApiCall { remoteDataSource.getMessages() })
        }.flowOn(Dispatchers.IO)
    }
}