package com.example.exam11.model

import com.google.gson.annotations.SerializedName

data class Message(
    val avatar: String,
    val email: String,
    @SerializedName("first_name")
    val firstName: String,
    val id: Int,
    @SerializedName("is_typing")
    val isTyping: Boolean,
    @SerializedName("last_message")
    val lastMessage: String,
    @SerializedName("last_name")
    val lastName: String,
    @SerializedName("message_type")
    val messageType: String,
    @SerializedName("unrea_message")
    val unreadMessage: Int,
    val updated_date: Long
)