package com.example.exam11

import android.app.Application
import android.content.Context
import dagger.hilt.android.HiltAndroidApp



@HiltAndroidApp
class MessengerApplication : Application() {

}