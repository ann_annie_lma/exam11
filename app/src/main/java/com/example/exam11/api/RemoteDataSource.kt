package com.example.exam11.api

import javax.inject.Inject


class RemoteDataSource @Inject constructor(private val messengerService: MessengerService) {

    suspend fun getMessages() = messengerService.getMessages()

}