package com.example.exam11.api

import com.example.exam11.model.Message
import com.example.exam11.util.Constants
import retrofit2.Response
import retrofit2.http.GET

interface MessengerService {

    @GET(Constants.ENDPOINT)
    suspend fun getMessages(): Response<List<Message>>
}