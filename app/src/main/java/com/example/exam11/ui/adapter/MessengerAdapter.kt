package com.example.exam11.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings.Global.getString
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.exam11.R
import com.example.exam11.databinding.MessengerItemBinding
import com.example.exam11.databinding.MessengerItemSecondaryBinding
import com.example.exam11.model.Message
import javax.inject.Inject
import javax.inject.Singleton


class MessengerAdapter(private  val context:Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object
    {
        private const val FIRST_ITEM = 1
        private const val SECOND_ITEM = 2
    }


    inner class MessengerViewHolderUnread(val binding: MessengerItemBinding) : RecyclerView.ViewHolder(binding.root)
    {

        fun bind()
        {
            binding.apply {
                val message = messages[adapterPosition]


                tvName.text = String.format(context.getString(R.string.full_name), message.firstName,message.lastName);

                tvCurrentMessage.text = message.lastMessage

                tvUnreadMessages.text = message.unreadMessage.toString()

                Glide.with(ivAvatar.getContext())
                    .load(message.avatar)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .centerCrop()
                    .into(ivAvatar)

            }
        }
    }

    inner class MessengerViewHolderTyping(val binding: MessengerItemSecondaryBinding) : RecyclerView.ViewHolder(binding.root)
    {

        fun bind()
        {
            binding.apply {
                val message = messages[adapterPosition]


                tvName.text = String.format(context.getString(R.string.full_name), message.firstName,message.lastName);

                tvCurrentMessage.text = message.lastMessage

                Glide.with(ivAvatar.getContext())
                    .load(message.avatar)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .centerCrop()
                    .into(ivAvatar)

            }
        }
    }


    private val diffCallBack = object: DiffUtil.ItemCallback<Message>()
    {
        override fun areItemsTheSame(oldItem: Message, newItem: Message): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Message, newItem: Message): Boolean {
            return oldItem == newItem
        }

    }

    private val differ = AsyncListDiffer(this,diffCallBack)
    var messages : List<Message>
        get() = differ.currentList
        set(value) {differ.submitList(value)}


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        if(viewType == FIRST_ITEM)
        {
            MessengerViewHolderUnread(
                MessengerItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false))
        } else
        {
            MessengerViewHolderTyping(
                MessengerItemSecondaryBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false))
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is MessengerViewHolderUnread)
            holder.bind()
        else if(holder is MessengerViewHolderTyping)
            holder.bind()
    }

    override fun getItemViewType(position: Int) =
        if(messages[position].isTyping) SECOND_ITEM else FIRST_ITEM

    override fun getItemCount() = messages.size
}