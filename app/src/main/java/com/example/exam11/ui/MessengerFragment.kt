package com.example.exam11.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.exam11.R
import com.example.exam11.databinding.FragmentMessengerBinding
import com.example.exam11.ui.adapter.MessengerAdapter
import com.example.exam11.ui.viewmodel.MessengerViewModel
import com.example.exam11.util.NetworkResult
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MessengerFragment : Fragment() {

    private var _binding: FragmentMessengerBinding? = null
    val binding get() = _binding!!

    private val viewModel by viewModels<MessengerViewModel>()
    private lateinit var messengerAdapter: MessengerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentMessengerBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUprecyclerView()
        fetchData()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setUprecyclerView()
    {
        val layoutManager = LinearLayoutManager(context)
        binding.rvMessenger.layoutManager = layoutManager

        messengerAdapter = MessengerAdapter(requireContext())
        binding.rvMessenger.adapter = messengerAdapter
    }

    private fun fetchData()
    {
        viewModel.fetchMessageResponse()
        viewModel.response.observe(viewLifecycleOwner)
        {
            when(it)
            {
                is NetworkResult.Success ->
                {
                    messengerAdapter.messages = it.data!!
                    binding.pbLoader.visibility = View.GONE
                }
                is NetworkResult.Error ->
                {
                    showError(it.message!!)
                    binding.pbLoader.visibility = View.GONE
                }
                is NetworkResult.Loading ->
                {
                    binding.pbLoader.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun showError(msg: String) {
        Snackbar.make(binding.root, msg, Snackbar.LENGTH_SHORT).show()
    }
}