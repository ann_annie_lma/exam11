package com.example.exam11.ui.viewmodel

import android.app.Application
import androidx.lifecycle.*
import com.example.exam11.model.Message
import com.example.exam11.reprository.MessengerRepository
import com.example.exam11.util.NetworkResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

@HiltViewModel
class MessengerViewModel @Inject constructor
    (
    private val repository: MessengerRepository,
    application: Application
) : AndroidViewModel(application) {

    private val _response: MutableLiveData<NetworkResult<List<Message>>> = MutableLiveData()
    val response: LiveData<NetworkResult<List<Message>>> = _response


    fun fetchMessageResponse() = viewModelScope.launch {
        repository.getMessages().collect { values ->
            _response.value = values
        }
    }
}